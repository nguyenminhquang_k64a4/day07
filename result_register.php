<?php
session_start();
?>

<html>
    
    <head>
        <meta charset='UTF-8'>
        <link rel='stylesheet' href='form_register.css'>
    </head>

    <body>
        
        <fieldset>

            <form>

                <table>

                    <tr>
                        <td class='td'><label>Họ và tên</label></td>
                        <?php echo "<td><label>".$_SESSION["name"]."</label></td>" ?>
                    </tr>

                    <tr>
                        <td class='td'><label>Giới tính</label></td>
                        <?php echo "<td><label>" . $_SESSION["gender"] . "</label></td>" ?>
                        
                    </tr>

                    <tr>
                        <td class='td'><label>Phân khoa</label></td>
                        <?php echo "<td><label>" . $_SESSION["faculty"] . "</label></td>" ?>
                    </tr>

                    <tr>
                        <td class='td'><label>Ngày sinh</label></td>
                        <?php echo "<td><label>" . $_SESSION["birth"] . "</label></td>" ?>
                    </tr>

                    <tr>
                        <td class='td'><label>Địa chỉ</label></td>
                        <?php echo "<td><label>" . $_SESSION["address"] . "</label></td>" ?>
                    </tr>

                    <tr>
                        <td class='td'><label>Hình ảnh</label></td>
                        <?php echo "<td><img src='". $_SESSION["file_upload"] . "' width = 100 /> </td>" ?>
                        
                    </tr>

                </table>

                <button name='submit' type='submit'> Xác nhận </button>

            </form>

        </fieldset>

    </body>

<html>